<?php

use common\models\Product;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Image */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="image-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?=
    $form->field($model, 'product_id')->dropDownList(
        ArrayHelper::map(Product::find()->select(['id', 'title'])->asArray()->all(), 'id', 'title')
    ) ?>

    <?php if(!$model->isNewRecord): ?>
        <?= Html::img($model->src)?>
    <?php endif;?>

    <?= $form->field($model, '_image')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
