<?php
namespace frontend\controllers;

use Yii;
use yii\base\Controller;
use yii\web\NotFoundHttpException;
use common\models\Product;
use common\models\Category;

class CatalogController extends Controller
{
    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $categoryId = Yii::$app->request->get('id');
        $categories = Category::find()->all();
        $category = null;

        if ($categoryId) {
            $category = Category::findOne($categoryId);
            if (is_null($category)) {
                throw new NotFoundHttpException('Category not found');
            }

            $products = $category->products;
        } else {
            $products = Product::find()->all();
        }

        return $this->render('index', [
            'category' => $category,
            'categories' => $this->getMenuItems($categories, $category ? $category->id : null),
            'products' => $products
        ]);
    }

    /**
     * @param Category[] $categories
     * @param null $activeId
     * @return array
     */
    private function getMenuItems($categories, $activeId = null)
    {
        $menuItems = [];
        foreach ($categories as $category) {
            if ($category->parent_id === null) {
                $menuItems[$category->id] = [
                    'active' => $activeId === $category->id,
                    'label' => $category->title,
                    'url' => ['catalog/index', 'id' => $category->id],
                ];
            } else {
                $this->placeCategory($category, $menuItems, $activeId);
            }
        }
        return $menuItems;
    }

    /**
     * Places category menu item into menu tree
     *
     * @param Category $category
     * @param $menuItems
     * @param int $activeId
     */
    private function placeCategory($category, &$menuItems, $activeId = null)
    {
        foreach ($menuItems as $id => $navLink) {
            if ($category->parent_id === $id) {
                $menuItems[$id]['items'][$category->id] = [
                    'active' => $activeId === $category->id,
                    'label' => $category->title,
                    'url' => ['catalog/index', 'id' => $category->id],
                ];
                break;
            } elseif (!empty($menuItems[$id]['items'])) {
                $this->placeCategory($category, $menuItems[$id]['items']);
            }
        }
    }

    /**
     * Returns IDs of category and all its sub-categories
     *
     * @param Category[] $categories all categories
     * @param int $categoryId id of category to start search with
     * @param array $categoryIds
     * @return array $categoryIds
     */
    private function getCategoryIds($categories, $categoryId, &$categoryIds = [])
    {
        foreach ($categories as $category) {
            if ($category->id == $categoryId || $category->parent_id == $categoryId) {
                $categoryIds[] = $category->id;
            }
            if (isset($categories[$categoryId]['items'])) {
                foreach ($categories[$categoryId]['items'] as $subCategoryId => $subCategory)
                    $this->getCategoryIds($categories, $subCategoryId, $categoryIds);
            }
        }
        return $categoryIds;
    }

}