<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'static'=>[
        'images'=>[
            'path'=>'@static/images',
            'web'=>'/static/images',
        ],
    ],
];
