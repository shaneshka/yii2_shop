<?php
/**
 * @var $faker \Faker\Generator
 * @var $index integer
 */

return [
    'title' => $faker->word,
    'slug' => $faker->word,
    'description' => $faker->sentence(10),
    'category_id' => $faker->numberBetween(1, 10),
    'price' => $faker->randomNumber(),
];
